# SPDX-FileCopyrightText: 2023 Brendan O'Leary for ProjectDiscovery
#
# SPDX-License-Identifier: MIT

# Nuclei Scan Graph IRL
# Runs on Adafruit MatrixPortal M4

import time
import random
import board
import microcontroller
import displayio
import terminalio
import busio
import adafruit_lis3dh
import digitalio
from adafruit_display_text.label import Label
from adafruit_display_text import bitmap_label
from adafruit_bitmap_font import bitmap_font
from adafruit_matrixportal.network import Network
from adafruit_matrixportal.matrix import Matrix

from fakedata import curve, random64
from spinnerled import spinnerled, spinnerled2, spinnerled3
from getnuxdata import getNuxScans, transform_array
import display_handlers
import input_handlers
import logging

BLINK = True
DEBUG = False
BLANK_ON_DRAW = True

# Get wifi details and more from a secrets.py file
try:
    from secrets import secrets
except ImportError:
    print("WiFi secrets are kept in secrets.py, please add them there!")
    raise

# --- Display setup ---
matrix = Matrix()
display = matrix.display
network = Network(status_neopixel=board.NEOPIXEL, debug=True)

# --- Drawing setup ---
group = displayio.Group()  # Create a Group
bitmap = displayio.Bitmap(64, 32, 2)  # Create a bitmap object,width, height, bit depth
color = displayio.Palette(5)  # Create a color palette
color[0] = 0x000000  # black background
#color[1] = 0xFF0000  # red
color[1] = (79,70,229)
color[2] = 0xCC4000  # amber
color[3] = 0x85FF00  # greenish
color[4] = (50, 50, 65)

#######
# Text screen control byte
#######
microcontroller.nvm[0] = 1

# --- Grid setup ---
tile_grid = displayio.TileGrid(bitmap, pixel_shader=color)
group.append(tile_grid)  # Add the TileGrid to the Group
display.show(group)

# --- Label setup ---
SM_FONT = bitmap_font.load_font("/fonts/55-10.bdf")
BIG_FONT = bitmap_font.load_font("/fonts/IBMPlexMono-Medium-24_jep.bdf")
SYS_FONT = terminalio.FONT
text_color = 0xCC4000

text_area = bitmap_label.Label(SM_FONT, text="Startup", color=text_color)
text_area.x = 1
text_area.y = 2
group.append(text_area)

# --- Draw and Clear display on startup ---
display_handlers.draw_loop(curve, bitmap, text_area, network)
display_handlers.blank_chart(bitmap)

last_check = None
numChecks = 0
logging.logevent("Nuclei Scan Graph IRL startup")

# --- Our forever loop ---
while True:
    input_handlers.check_button(text_area, network)
    if DEBUG:
        print("last_check: " + str(last_check))
        print("time.monotonic(): " + str(time.monotonic()))
    if last_check is None or time.monotonic() > last_check + 15:
        try:
            if DEBUG:
                print("Loop A")
            network.get_local_time()  # Synchronize Board's clock to Internet
            numChecks += 1
            rawArr = getNuxScans()
            if rawArr != None:
                newArr = transform_array(rawArr)
                if DEBUG:
                    print(newArr)
                last_check = time.monotonic()
                display_handlers.draw_loop(newArr, bitmap, text_area, network)
            else:
                display_handlers.handle_label("Data Error", text_area, network)
                logging.logevent("Error getting data", "ERROR", "DATAFETCH")
            logging.logevent(
                f"Number of checks since start: {str(numChecks)}", 
                level="INFO", 
                type="CYCLE", 
                numeric=numChecks
            )
        except RuntimeError as e:
            print("Some error occured, retrying! -", e)
            logging.logevent("Runtime Error", "ERROR")

    display_handlers.binary_spin(bitmap)

    if DEBUG:
        print("Loop B")
        print(time.monotonic() - last_check)

    time.sleep(1)
