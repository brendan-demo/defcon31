import time
import adafruit_requests as requests

from secrets import secrets

#   -d "this is the message"

feed = "test"

def postdata(numChecks):
    print('sending count: ', numChecks)

    # --- AIO ---
    post_data = { "value": numChecks }
    post_url = "https://io.adafruit.com/api/v2/" + secrets["aio_username"] + "/feeds/" + feed + "/data"
    print("post to %s" % post_url)
    requests.post(
        post_url,
        data=post_data,
        headers={"X-AIO-KEY": secrets["aio_key"]}
    )

    # --- Upstash Kafaka ---
    post_url = secrets["UPSTASH_KAFKA_REST_URL"] + "/webhook?topic=livefeed&user=" + secrets["UPSTASH_KAFKA_REST_USERNAME"] + "&pass=" + secrets["UPSTASH_KAFKA_REST_PASSWORD"]
    requests.post(
        post_url,
        data="Current count: " + str(numChecks)
    )
    
    