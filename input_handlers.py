import digitalio
import board
import microcontroller

import display_handlers
import logging

max_screens = 2 # 0, 1, 2 = three screens, max of 2
INPUT_DEBUG = False

##########################
#
# Button setup
#
##########################

button_down = digitalio.DigitalInOut(board.BUTTON_DOWN)
button_down.switch_to_input(pull=digitalio.Pull.UP)

button_up = digitalio.DigitalInOut(board.BUTTON_UP)
button_up.switch_to_input(pull=digitalio.Pull.UP)

def check_button(text_area, network):
    if not button_down.value:
        logging.logevent("Down Button Pressed", type="input_handlers.py")
        curr = microcontroller.nvm[0]
        curr -= 1
        if curr < 0:
            curr = 0
        microcontroller.nvm[0] = curr
    elif not button_up.value:
        logging.logevent("Up Button Pressed", type="input_handlers.py")
        curr = microcontroller.nvm[0]
        curr += 1
        if curr > max_screens:
            curr = max_screens
        microcontroller.nvm[0] = curr
    if INPUT_DEBUG:
        print(microcontroller.nvm[0])
    display_handlers.handle_label(None, text_area, network)