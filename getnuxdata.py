import adafruit_requests as requests

API_DEBUG = False
BASE_URL = "https://nux-graphs.vercel.app"
SCAN_URL = f"{BASE_URL}/nuclei-scans.json"
LAST_65_URL = f"{BASE_URL}/nuclei-scans-last-65.json"

def getNuxScans():
    try:
        print("Fetching data from" + LAST_65_URL)
        response = requests.get(LAST_65_URL)
        print("-" * 40)
        json = response.json()
        if API_DEBUG:
            print("JSON Response", json)
            print("-" * 40)
        response.close()
        return json
    except:
        print("An exception occured in getNuxScans")
        return None

def transform_array(arr):
    min_val = min(arr)
    max_val = max(arr)
    scaled_arr = [(x - min_val) / (max_val - min_val) for x in arr]
    transformed_arr = [int(x * 21 + 1) for x in scaled_arr]
    return transformed_arr