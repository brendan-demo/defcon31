import time
import adafruit_requests as requests

from secrets import secrets

LOG_DEBUG = True
SUPABASE_URL = "https://slqqilucsehnaahnisqr.supabase.co/rest/v1/logging"

def logevent(message, level="INFO", type="GENERIC", numeric=0):
    print(message)

    post_data = { "message": message, "level": level, "type": type, "numeric": str(numeric) }
    if LOG_DEBUG:
        print(post_data)

    try:
        response = requests.post(
            SUPABASE_URL,
            data=post_data,
            headers={
                "apikey": secrets["SUPABASE_API_KEY"],
                "Authorization": "Bearer" + secrets["SUPABASE_API_KEY"],
                #"Content-Type": "application/json",
                "Perfer": "return=minimal"
            }
        )

        if LOG_DEBUG:
            print(response.json())
        response.close()
    except Exception as e:
        print("Exception in logging")
        print(e)