import microcontroller
import time

from spinnerled import spinnerled, spinnerled2, spinnerled3
import logging

DISPLAY_DEBUG = False
BLANK_ON_DRAW = True
currLEDspin = 0
text = "Welcome!"

# --- Loop for drawing the graph ---
def draw_loop(draw_data, bitmap, text_area, network):
    if DISPLAY_DEBUG:
        print("Enter draw loop")
        print("Clear graph")
    handle_label("Loading...", text_area, network)
    if BLANK_ON_DRAW:
        blank_chart(bitmap)
    if DISPLAY_DEBUG:
        print("Print graph")
    for x in range(0, 64):
        if (x % 2) == 0:
            binary_spin(bitmap)
        if x < len(draw_data) - 1:
            for y in range(0, 21):
                if draw_data[x] >= y + 1:
                    bitmap[x, 31-y] = 1
                else:
                    bitmap[x, 31-y] = 0
                time.sleep(0.0009)
    handle_label("Scans", text_area, network)
    if DISPLAY_DEBUG:
        print("End draw loop")

def binary_spin(bitmap):
    global currLEDspin
    for i, x in enumerate(spinnerled2[currLEDspin]):
        for j, y in enumerate(x):
            # j is the X axis; i is the Y
            pixel = j + 57, i
            if DISPLAY_DEBUG:
                print(f"pixel: {pixel}, y: {y}")
            bitmap[pixel] = y
    currLEDspin += 1
    if currLEDspin > len(spinnerled2) - 1:
        currLEDspin = 0

def handle_label(newtext, text_area, network):
    global text
    try:
        if newtext != None:
            text = newtext
        if network.ip_address == "0.0.0.0":
            # text_area.text = secrets['ssid']
            text_area.text = "Wifi..."
        else:
            curr_screen = microcontroller.nvm[0]
            if curr_screen == 0:
                text_area.text = network.ip_address
            elif curr_screen == 1:
                text_area.text = text
            elif curr_screen == 2:
                text_area.text = "Screen 2"
    except:
        logging.logevent("Error in handle_label", "ERROR", "display_handlers.py")

def blank_chart(bitmap):
    for x in range(0,64):
        for y in range(0, 32):
            bitmap[x, y] = 0